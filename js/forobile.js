$(document).ready(function () {
    $("li.menu-item").hover(function () {
        $(this).find('.sub-menu-box').fadeIn(300);
        $(this).find('.sub-menu-box').addClass('d-block');
        $(this).find('.sub-menu-box').removeClass('d-none');
    }, function () {
        $(this).find('.sub-menu-box').removeClass('d-block');
        $(this).find('.sub-menu-box').addClass('d-none');
        $(this).find('.sub-menu-box').fadeOut(300);
    });
});
$(document).ready(function () {
    $(".owl-carousel").owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,
        center: true,
        nav: true,
        dots: true,
        responsive: {
            0: {
                nav: false,
            },
            600: {
                nav: false,
            },
            1000: {
                items: 1,
            }
        }
    });
});
$(document).ready(function () {
    $(".owl-carousel-2").owlCarousel({
        items: 6,
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 2,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 6,
            }
        }
    });
});

function myFunction() {
    var x = $('#menu-mobile-show');
    if (x.css('right') == '-300px') {
        x.css('right', '0');
        $(".background-shadow").css("display", "block");
        $('#body').css('overflow', 'hidden');
    } else {
        x.css('right', '-300px');
        $(".background-shadow").css("display", "none");
        $('#body').css('overflow', 'vissible');
    }
}

function myFunction2() {
    var x = $('#menu-mobile-show');
    if (x.css('right') == '0') {
        x.css('right', '-300px');
        $(".background-shadow").css("display", "none");
    } else {
        x.css('right', '-300px');
        $(".background-shadow").css("display", "none");
    }
}

function myFunction3() {
    var x = $('#item1');
    if (x.css('display') == "none") {
        x.css('display', 'block');
        x.css('border-top', '1px solid red');
    } else {
        x.css('display', 'none');
    }
}

function myFunction4() {
    var x = $('#item2');
    if (x.css('display') == "none") {
        x.css('display', 'block');
        x.css('border-top', '1px solid red')
    } else {
        x.css('display', 'none');
    }
}

function myFunction5() {
    var x = $('#item3');
    if (x.css('display') == "none") {
        x.css('display', 'block');
        x.css('border-top', '1px solid red')
    } else {
        x.css('display', 'none');
    }
}

function myFunction6() {
    var x = $('#item4');
    if (x.css('display') == "none") {
        x.css('display', 'block');
        x.css('border-top', '1px solid red')
    } else {
        x.css('display', 'none');
    }
}

function myFunction7() {
    var x = $('#item5');
    if (x.css('display') == "none") {
        x.css('display', 'block');
        x.css('border-top', '1px solid red')
    } else {
        x.css('display', 'none');
    }
}

function myFunction8() {
    var x = $('#item6');
    if (x.css('display') == "none") {
        x.css('display', 'block');
        x.css('border-top', '1px solid red')
    } else {
        x.css('display', 'none');
    }
}

function myFunction9() {
    var x = $('#menu-mobile-show');
    if (x.css('right') == '0') {
        x.css('right', '-300px');
        $(".background-shadow").css("display", "none");
    } else {
        x.css('right', '-300px');
        $(".background-shadow").css("display", "none");
    }
}

$(document).ready(function () {
    var a = $(".background-shadow")
    if (a.css('display') == 'block') {
        $('#body').css('overflow', 'hidden');
    } else {
        $('#body').css('overflow', 'vissible');
    }
});
var myModal = document.getElementById('myModal')
var myInput = document.getElementById('myInput')

myModal.addEventListener('show.bs.modal', function (event) {
    // Button that triggered the modal
    var button = event.relatedTarget
    // Extract info from data-bs-* attributes
    var title = button.getAttribute('data-title')
    var code = button.getAttribute('data-code')
    var category = button.getAttribute('data-category')
    var producer = button.getAttribute('data-producer')
    var status = button.getAttribute('data-status')
    var off = button.getAttribute('data-off')
    var price = button.getAttribute('data-price')
    var description = button.getAttribute('data-description')
    var image = button.getAttribute('data-image')
    // If necessary, you could initiate an AJAX request here
    // and then do the updating in a callback.
    //
    // Update the modal's content.
    var modalTitle = myModal.querySelector('.modal-title')
    var productTitle = myModal.querySelector('.product-title')
    var productCode = myModal.querySelector('.product-code')
    var productCategory = myModal.querySelector('.product-category')
    var productProducer = myModal.querySelector('.product-producer')
    var productStatus = myModal.querySelector('.product-status')
    var productOff = myModal.querySelector('.product-off')
    var productPrice = myModal.querySelector('.product-price')
    var productDescription = myModal.querySelector('.product-description')
    var productImg = myModal.querySelector('.product-img')

    /*
        modalTitle.textContent = 'New message to ' + recipient
    */
    productTitle.textContent = title
    productCode.textContent = code
    productCategory.textContent = category
    productProducer.textContent = producer
    productStatus.textContent = status
    productOff.textContent = off
    productPrice.textContent = price
    productDescription.textContent = description
    productImg.src = image
    modalTitle.textContent = title + '(یک عدد) '
})